import unittest
import platform

from ufterm.utils import *


class RegisterTest(unittest.TestCase):

    def test_noop(self):
        self.assertIsNone(noop())
        self.assertIsNone(noop("toto"))
        self.assertIsNone(noop(1, "toto"))

    def test_get_program_file_x86_location(self):
        if platform.system() == "Windows":
            self.assertEqual(r"C:\Program Files (x86)", get_program_file_x86_location())

    def test_is_empty_str(self):
        self.assertTrue(is_empty_str(""))
        self.assertTrue(is_empty_str(" "))
        self.assertTrue(is_empty_str("\t"))
        self.assertTrue(is_empty_str("\n"))
        self.assertTrue(is_empty_str("\r"))
        self.assertTrue(is_empty_str("\r\n"))
        self.assertTrue(is_empty_str("   "))
        self.assertTrue(is_empty_str("\t\t"))

    def test_get_digit_number(self):
        self.assertEqual(1, get_digit_number(0))
        self.assertEqual(1, get_digit_number(1))
        self.assertEqual(1, get_digit_number(9))
        self.assertEqual(2, get_digit_number(10))
        self.assertEqual(2, get_digit_number(99))
        self.assertEqual(6, get_digit_number(634354))
        self.assertEqual(3, get_digit_number(-99))

    def test_write_file_content(self):
        write_file_content("My super content", "test/file_name.txt")
        self.assertTrue(os.path.isdir("test"))
        self.assertTrue(os.path.isfile("test/file_name.txt"))
        os.remove("test/file_name.txt")
        os.rmdir("test")

        write_file_content("My super content", "double/test/file_name.txt")
        self.assertTrue(os.path.isdir("double"))
        self.assertTrue(os.path.isdir("double/test"))
        self.assertTrue(os.path.isfile("double/test/file_name.txt"))
        os.remove("double/test/file_name.txt")
        os.rmdir("double/test")
        os.rmdir("double")

    def test_replace_forbidden_file_name_char(self):
        self.assertEqual("GW.01-001", replace_forbidden_file_name_char("GW?01*001"))
        self.assertEqual("GW.01-001", replace_forbidden_file_name_char("GW?01:001"))
        self.assertEqual("GW-01-001", replace_forbidden_file_name_char("GW\\01\\001"))
        self.assertEqual("GW''01''001", replace_forbidden_file_name_char('GW"01"001'))
        self.assertEqual("(GW-01-001)", replace_forbidden_file_name_char("<GW/01/001>"))
        self.assertEqual("GW, 01, 001", replace_forbidden_file_name_char("GW|01|001"))

    def test_str_is_integer(self):
        self.assertTrue(str_is_integer("0"))
        self.assertTrue(str_is_integer("6465"))
        self.assertFalse(str_is_integer(" "))
        self.assertFalse(str_is_integer("0 "))
        self.assertFalse(str_is_integer(" 0"))
        self.assertFalse(str_is_integer(" 0 "))
        self.assertFalse(str_is_integer("6465 "))

    def test_str_is_float(self):
        self.assertTrue(str_is_float("0"))
        self.assertTrue(str_is_float("0."))
        self.assertTrue(str_is_float("0.4"))
        self.assertTrue(str_is_float("6465"))
        self.assertTrue(str_is_float("6465."))
        self.assertTrue(str_is_float(".6465"))
        self.assertTrue(str_is_float("8.165"))
        self.assertFalse(str_is_float(""))
        self.assertFalse(str_is_float(" "))
        self.assertFalse(str_is_float("0 "))
        self.assertFalse(str_is_float(" 0"))
        self.assertFalse(str_is_float(" 0 "))
        self.assertFalse(str_is_float("6465 "))
        self.assertFalse(str_is_float(" 6465"))
        self.assertFalse(str_is_float(" 6465 "))

    def test_convert_str_to_type(self):
        self.assertEqual("", convert_str_to_type("", str))
        self.assertEqual("s", convert_str_to_type("s", str))
