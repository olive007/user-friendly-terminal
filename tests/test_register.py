import unittest
from freezegun import freeze_time

from ufterm.register import *

TEST_FOLDER = os.path.dirname(__file__)


class RegisterTest(unittest.TestCase):

    def setUp(self):
        Register.clear()

    def test_singleton(self):
        register1 = Register()
        register2 = Register()
        self.assertRaises(KeyError, lambda: Register("key"))

        self.assertIsNotNone(register1)
        self.assertIsNotNone(register2)

        self.assertEqual(id(register1), id(register2))
        self.assertEqual(register1, register2)

    def test_set_attr_meta(self):
        Register.toto = "value"
        self.assertEqual("value", Register("toto"))
        self.assertEqual("value", Register["toto"])
        self.assertEqual("value", Register.toto)

    def test_set_attr(self):
        Register().toto = "value"
        self.assertEqual("value", Register("toto"))
        self.assertEqual("value", Register["toto"])
        self.assertEqual("value", Register.toto)

    def test_set_item_meta(self):
        Register["toto"] = "value"
        self.assertEqual("value", Register("toto"))
        self.assertEqual("value", Register["toto"])
        self.assertEqual("value", Register.toto)

    def test_set_item(self):
        Register()["toto"] = "value"
        self.assertEqual("value", Register("toto"))
        self.assertEqual("value", Register["toto"])
        self.assertEqual("value", Register.toto)

    def test_get_none_value(self):
        Register["toto"] = None
        self.assertIsNone(Register("toto"))
        self.assertIsNone(Register["toto"])
        self.assertIsNone(Register.toto)

    def test_reset(self):
        tmp = Register()
        tmp.key1 = "value1"
        tmp.key2 = "value2"

        self.assertEqual("value1", tmp.key1)
        self.assertEqual("value1", tmp["key1"])
        self.assertEqual("value2", tmp.key2)
        tmp.reset()
        self.assertRaises(KeyError, lambda: tmp["key1"])
        self.assertRaises(KeyError, lambda: tmp.key1)
        self.assertRaises(KeyError, lambda: tmp["key2"])
        self.assertRaises(KeyError, lambda: tmp.key2)
        self.assertEqual("{}", str(tmp))

    def test_str(self):
        tmp = Register()
        self.assertEqual("{}", str(tmp))
        self.assertEqual("{}", str(Register()))

    def test_reset_right_away(self):
        Register.reset()
        self.assertEqual("{}", str(Register()))

    def test_key_not_found(self):
        tmp = Register()
        self.assertRaises(KeyError, lambda: Register('toto'))
        self.assertRaises(KeyError, lambda: Register['toto'])
        self.assertRaises(KeyError, lambda: Register.toto)
        self.assertRaises(KeyError, lambda: tmp["toto"])
        self.assertRaises(KeyError, lambda: tmp.toto)

    def test_reset_meta(self):
        Register.key1 = "value1"
        Register.key2 = "value2"

        self.assertEqual("value1", Register.key1)
        self.assertEqual("value2", Register.key2)
        Register.reset()
        self.assertRaises(KeyError, lambda: Register("key1"))
        self.assertRaises(KeyError, lambda: Register["key1"])
        self.assertRaises(KeyError, lambda: Register.key1)
        self.assertRaises(KeyError, lambda: Register("key2"))
        self.assertRaises(KeyError, lambda: Register["key2"])
        self.assertRaises(KeyError, lambda: Register.key2)
        self.assertEqual("{}", str(Register()))

    def test_get(self):
        Register.get("key1")
        Register.get("key2", [])
        Register.get("key3", ["tata"])
        Register.get("key4", {})
        Register.get("key5", {'sub_key': "sub_value"})
        self.assertIsNone(Register.key1)
        self.assertIsNotNone(Register.key2)
        self.assertEqual(0, len(Register.key2))
        self.assertEqual(1, len(Register.key3))
        self.assertEqual("tata", Register.key3[0])
        self.assertEqual(0, len(Register.key4.keys()))
        self.assertEqual(1, len(Register.key5.keys()))
        self.assertEqual("sub_value", Register.key5.sub_key)

    def test_get2(self):
        Register.get("my_dict", {}).get("key", [])
        self.assertEqual(0, len(Register.my_dict.key))
        Register.my_dict.key.append("toto")
        self.assertEqual(1, len(Register.my_dict.key))


class RegisterInsideHomeTest(unittest.TestCase):

    def setUp(self):
        Register.clear()
        self.now = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        if sys.platform == 'win32':
            self.real_file_path = os.path.join(os.getenv('userprofile'), f"unittest_uft_{self.now}.yml")
        else:
            self.real_file_path = os.path.join(os.getenv('HOME'), f"unittest_uft_{self.now}.yml")
        if os.path.isfile(self.real_file_path):
            os.remove(self.real_file_path)

    def test_record_in_file(self):
        Register.key1 = "VALUE1"
        Register.key2 = "VALUE2"

        Register.record_in_file("~unittest_uft_%s.yml" % self.now)
        self.assertEqual(f"~unittest_uft_{self.now}.yml", Register.__class__._file_path)
        Register.record_in_file("~\\unittest_uft_%s.yml" % self.now)
        self.assertEqual(self.real_file_path, Register.__class__._file_path)
        Register.record_in_file("~/unittest_uft_%s.yml" % self.now)
        self.assertEqual(self.real_file_path, Register.__class__._file_path)
        Register.record_in_file("~/unittest_uft_%s.yml" % self.now)
        self.assertEqual(self.real_file_path, Register.__class__._file_path)
        Register().wait_commit_to_finish()
        self.assertTrue(os.path.isfile(self.real_file_path))

    def tearDown(self):
        if os.path.isfile(self.real_file_path):
            os.remove(self.real_file_path)
        if os.path.isfile(f"~unittest_uft_{self.now}.yml"):
            os.remove(f"~unittest_uft_{self.now}.yml")



class RegisterInsideFolderTest(unittest.TestCase):

    def setUp(self):
        Register.clear()
        now = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        self.folder_path = "folder_%s" % now
        if os.path.isdir(self.folder_path):
            os.rmdir(self.folder_path)
        self.file_path = os.path.join(self.folder_path, "unitest_uft_%s.yml" % now)
        if os.path.isfile(self.file_path):
            os.remove(self.file_path)

    def test_record_in_file(self):
        Register.key1 = "VALUE1"
        Register.key2 = "VALUE2"

        Register.record_in_file(self.file_path)
        self.assertEqual(self.file_path, Register.__class__._file_path)
        Register.wait_commit_to_finish()
        self.assertTrue(os.path.isdir(self.folder_path))
        self.assertTrue(os.path.isfile(self.file_path))

    def tearDown(self):
        if os.path.isfile(self.file_path):
            os.remove(self.file_path)
        if os.path.isdir(self.folder_path):
            os.rmdir(self.folder_path)


class RegisterInsideSubFolderTest(unittest.TestCase):

    def setUp(self):
        Register.clear()
        now = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        self.folder_path = "folder_%s" % now
        if os.path.isdir(self.folder_path):
            os.rmdir(self.folder_path)
        self.file_path = os.path.join(self.folder_path, "sub", "unitest_uft_%s.yml" % now)
        if os.path.isfile(self.file_path):
            os.remove(self.file_path)

    def test_record_in_file(self):
        Register.key1 = "VALUE1"
        Register.key2 = "VALUE2"

        Register.record_in_file(self.file_path)
        self.assertEqual(self.file_path, Register.__class__._file_path)
        Register.wait_commit_to_finish()
        self.assertTrue(os.path.isdir(self.folder_path))
        self.assertTrue(os.path.isdir(os.path.join(self.folder_path, "sub")))
        self.assertTrue(os.path.isfile(self.file_path))

    def tearDown(self):
        if os.path.isfile(self.file_path):
            os.remove(self.file_path)
        if os.path.isdir(os.path.join(self.folder_path, "sub")):
            os.rmdir(os.path.join(self.folder_path, "sub"))
        if os.path.isdir(self.folder_path):
            os.rmdir(self.folder_path)


class RegisterInsideFileTest(unittest.TestCase):

    def setUp(self):
        Register.clear()
        now = datetime.now().strftime("%y-%m-%d_%H-%M-%S")
        source_file = os.path.join(TEST_FOLDER, "persistent.yml")
        self.file_path = os.path.join(TEST_FOLDER, "testing_persistent_%s.yml" % now)

        with open(source_file, 'rb') as src, open(self.file_path, 'wb') as dst:
            while True:
                copy_buffer = src.read(1024 * 1024)
                if not copy_buffer:
                    break
                dst.write(copy_buffer)

    def test_record_in_file(self):
        Register.key1 = "VALUE1"
        Register.key2 = "VALUE2"
        Register.record_in_file(self.file_path)
        Register.key4 = "VALUE4"

        self.assertEqual("VALUE1", Register.key1)
        self.assertEqual("VALUE2", Register.key2)
        self.assertRaises(KeyError, lambda: Register.key3)
        self.assertEqual("VALUE4", Register.key4)
        self.assertEqual("persistent value 1", Register.persistant_key1)
        self.assertEqual("persistent value 2", Register.persistant_key2)
        self.assertEqual("wrong value", Register.update_key)
        Register.update_key = "updated value"
        self.assertEqual("updated value", Register.update_key)
        Register.wait_commit_to_finish()
        self.assertTrue(os.path.isfile(self.file_path))

    def test_record_in_file_directly(self):
        Register.record_in_file(self.file_path)
        Register.key1 = "VALUE1"
        Register.key2 = "VALUE2"

        self.assertEqual("VALUE1", Register.key1)
        self.assertEqual("VALUE2", Register.key2)
        self.assertRaises(KeyError, lambda: Register.key3)
        self.assertEqual("persistent value 1", Register.persistant_key1)
        self.assertEqual("persistent value 2", Register.persistant_key2)
        self.assertEqual("wrong value", Register.update_key)
        Register.update_key = "updated value"
        self.assertEqual("updated value", Register.update_key)
        Register.wait_commit_to_finish()
        self.assertTrue(os.path.isfile(self.file_path))

    @freeze_time('2175-08-15 18:42:27')
    def test_record_in_file_content(self):
        Register.record_in_file(self.file_path)
        self.assertTrue(os.path.isfile(self.file_path))

        Register.key1 = "new-value1"
        Register.key2 = "new-value2"

        self.assertEqual("new-value1", Register.key1)
        self.assertEqual("new-value2", Register.key2)
        self.assertEqual("wrong value", Register.update_key)

        content = "# File wrote by the user-friendly-terminal library at 18:42:27 on the 2175-08-15\n"
        content += "key1: new-value1\n"
        content += "key2: new-value2\n"
        content += "persistant_key1: persistent value 1\n"
        content += "persistant_key2: persistent value 2\n"
        content += "update_key: wrong value\n"

        Register.wait_commit_to_finish()
        with open(self.file_path) as fp:
            content_read = fp.read()
            self.assertEqual(content, content_read)

    @freeze_time('2175-08-15 18:42:27')
    def test_record_in_file_content_with_specific_order(self):
        Register.record_in_file(self.file_path)
        self.assertTrue(os.path.isfile(self.file_path))

        Register.z_key = "last_value"
        Register.a_key = "first_value"
        Register.get('my_dict', {
            'z': "value1",
            'a': "value2",
            'p': "value3",
            'b': "value4"
        })

        self.assertEqual("first_value", Register.a_key)
        self.assertEqual("last_value", Register.z_key)
        self.assertEqual("wrong value", Register.update_key)
        content = "# File wrote by the user-friendly-terminal library at 18:42:27 on the 2175-08-15\n"
        content += "a_key: first_value\n"
        content += "my_dict:\n"
        content += "  z: value1\n"
        content += "  a: value2\n"
        content += "  p: value3\n"
        content += "  b: value4\n"
        content += "persistant_key1: persistent value 1\n"
        content += "persistant_key2: persistent value 2\n"
        content += "update_key: wrong value\n"
        content += "z_key: last_value\n"

        Register.wait_commit_to_finish()
        with open(self.file_path) as fp:
            content_read = fp.read()
            self.assertEqual(content, content_read)

    def tearDown(self) -> None:
        if os.path.isfile(self.file_path):
            os.remove(self.file_path)
