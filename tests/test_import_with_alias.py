from freezegun import freeze_time

from custom_unit_test_class import PrintingUnitTestCase

import ufterm as uft


class AliasImportTest(PrintingUnitTestCase):

    @freeze_time('2175-08-15 18:42:27')
    def test_print(self):
        self.assertPrintEquals(("irrelevant message", ""), lambda: print("irrelevant message"))
        self.assertPrintEquals(("[18:42:27] irrelevant message", ""), lambda: uft.show("irrelevant message"))
