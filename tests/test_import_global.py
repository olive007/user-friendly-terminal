import unittest
from freezegun import freeze_time

from custom_unit_test_class import PrintingUnitTestCase

from ufterm import *


class GlobalImportTest(PrintingUnitTestCase):

    @freeze_time('2175-08-15 18:42:27')
    def test_print(self):
        self.assertPrintEquals(("irrelevant message", ""), lambda: print("irrelevant message"))
        self.assertPrintEquals(("[18:42:27] irrelevant message", ""), lambda: show("irrelevant message"))

    @freeze_time('2175-08-15 18:42:27')
    def test_print(self):
        print = show
        self.assertPrintEquals(("[18:42:27] irrelevant message", ""), lambda: print("irrelevant message"))
        self.assertPrintEquals(("[18:42:27] irrelevant message", ""), lambda: show("irrelevant message"))
